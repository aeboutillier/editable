

const table = document.getElementById("table");
const lien = document.getElementById("lien");
const input = document.getElementById('filtre');
const ajout = document.getElementById("ajout");

let objTab;

// ----------------------------------------------------------------------------------------------------------- Selection Tableau --
lien.addEventListener('change', selectTab);

function selectTab(event) {

    tableau = event.target.value;

    //---------------> FETCH
    fetch(tableau)                                  // j'envoie ma requête
        .then(                                      // quand je reçois la réponse
            (response) => response.json()           // je récupère le JSON de la réponse
        )
        .then(                                      // quand c'est pret
            (tab) => {
                objTab = tab;                       // je mets mes data dans une variable

                affichage(objTab);                  // je lance ma fonction d'affichage
            }
        ).catch(
            (err) => {
                console.log(err);                   // j'affiche les erreurs HTTP au cas où
            }
        );

}


function affichage(tab) {

    // ------------------------------------------------------------------------------------------------------- Création En-Tête --
    table.innerHTML = '<div id="tete"></div>';
    const en_tete = document.getElementById("tete");
    en_tete.addEventListener('click', trie);


    while (ajout.firstChild) {
        ajout.removeChild(ajout.firstChild);        // Clean de la Div "Ajout"
    }

    // ------------------------------------------------------------------------------------------------------- Création Ligne d'Ajout --
    const addbox = document.createElement("div");
    addbox.setAttribute("id", "ligneAjout");
    ajout.appendChild(addbox);

    const addbtn = document.createElement("div");
    addbtn.setAttribute("id", "addBTN");
    addbtn.innerHTML = "AJOUTER";
    addbox.appendChild(addbtn);


    let tabCle = tab[0];
    let cle = Object.keys(tabCle);


    cle.forEach(element => {

        // ------------------------------------------------------- Remplissage En-Tête --
        const newHead = document.createElement("h2");
        newHead.setAttribute("id", element);
        newHead.innerHTML = element;
        en_tete.appendChild(newHead);

        // ------------------------------------------------------- Remplissage Ligne d'Ajout --
        const newData = document.createElement("p");
        newData.setAttribute("contenteditable", "true");
        newData.classList.add("addData");
        newData.innerHTML = element + "...";
        newData.setAttribute("value", element);
        addbox.appendChild(newData);

    });


    for (let obj of tab) {

        // ------------------------------------------------------------------------------------------------------- Création Ligne Data --
        const box = document.createElement("div");
        box.classList.add("ligne");
        table.appendChild(box);


        for (let value in obj) {

            // ---------------------------------------------------- Remplissage Data --
            const newContent = document.createElement("p");

            newContent.setAttribute("contenteditable", "true");
            newContent.classList.add("data");
            newContent.innerHTML = obj[value];
            box.appendChild(newContent);

        }
    }

    // ---------------------------------------------------------------------------------------------------------------- Ajout Tableau --
    const addBTN = document.getElementById("addBTN");
    addBTN.addEventListener('click', add_Value);

    function add_Value(event) {

        const addData = document.getElementsByClassName("addData");

        let addObj = {}

        for (let e of addData) {

            let cle = e.getAttribute("value");
            let data = e.textContent;

            addObj[cle] = data;

        }

        objTab.push(addObj);
        affichage(objTab);

    }
}


// ---------------------------------------------------------------------------------------------------------------- Recherche Tableau --
input.addEventListener('keyup', search_Value);

function search_Value(event) {

    let recherche = event.target.value;

    const data = document.getElementsByClassName("ligne");

    for (let e of data) {

        if (e.textContent.toLowerCase().includes(recherche)) { e.style.display = "flex"; }
        else { e.style.display = "none"; }
    }

}


// ---------------------------------------------------------------------------------------------------------------- Trie Tableau --
function trie(event) {

    let nomCle = event.target.textContent;

    const AlphaNum = (a, b) => {
        if (a[nomCle] > b[nomCle]) return 1;
        if (a[nomCle] < b[nomCle]) return -1;

        return 0;
    };

    let monTab;

    if (event.detail = 1) { monTab = objTab.sort(AlphaNum); }
    if (event.detail > 1) { monTab = objTab.sort(AlphaNum).reverse(); }
    console.log(event.detail);
    affichage(monTab);

}

