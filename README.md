# Documentation --> EDITABLE <--

---

---

### 1. [Infos-Générales]

---

**Editable** est un tableau éditable qui permet de modifier un ensemble d’objets homogènes fournis dans un tableau en JSON.
Il permet d'afficher un ou plusieurs tableau JSON, le/les modifer, et le/les sauvegarder.

---

---

### 2. [Technologies]

---

Pour cette application, on utilise pricipalement du **JavaScript**, qui va générer la structure **HTML** d'un tableau en fonction d'un/des fichiers **JSON**, ainsi que du **PHP** pour sauvegarder les modifications éventuelles,et enfin du **CSS** pour la mise en forme.

---

---

### 3. [Installation]

---

> Récupération du Dépot Git

---

> - Ouvrir le lien du projet sur [**Git Lab**](https://gitlab.com/users/aeboutillier/projects)
> - Cloner le projet sur votre serveur (LAMP/WAMP/...) voir [**Cloner sur Git Hub**](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
> - Insérer votre/vos tableaux JSON dans le dossier --> _DATA_
> - Dans --> _index.html_. Ajouter a la suite des autres, une balise "option" contenant le chemin d'accès du fichier JSON.  

```html
<select id = "lien">
                    <option> ↴ </option>
                    <option value="../data/person.json"> Person </option>
                    <option value="../data/points.json"> Points </option>
                    <option value="../data/product.json"> Product </option>
                    <!-- AJOUTER ICI -->
                </select>
```

---

---

### 4. [Fonctionnalitées]

---

> - Affichage des données d'un tableau JSON.
> - Cellules du tableau éditable.
> - Recherche/Filtre des données.
> - Tri des données pour chaque colonne (croissant et décroissant).
> - Ajout de donnée / création de nouvel objet.
> - ~Sauvegarde des modifications.~

---

---

